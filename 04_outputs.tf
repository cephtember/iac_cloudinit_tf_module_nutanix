output "master_bind9_node" {
  value = nutanix_virtual_machine.master_bind9_node
}

output "master_loadbalancer_node" {
  value = nutanix_virtual_machine.master_loadbalancer_node
}

output "master_control_plane_node" {
  value = nutanix_virtual_machine.master_control_plane_node
}

output "additional_control_plane_nodes" {
  value = nutanix_virtual_machine.additional_control_plane_nodes
}

output "master_worker_node" {
  value = nutanix_virtual_machine.master_worker_node
}

output "additional_worker_nodes" {
  value = nutanix_virtual_machine.additional_worker_nodes
}